import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

/**
 * Stylesheets
 */
import 'vue-select/dist/vue-select.css';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';
import './assets/scss/app.scss';

// Install BootstrapVue
import { BootstrapVue } from 'bootstrap-vue';
Vue.use(BootstrapVue);
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Mixins
import notifications from './mixins/notifications';
Vue.mixin(notifications);

/**
 * External packages
 */
// vuelidate for validation
import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

// vselect for searchable select
import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';
Vue.component('v-select', vSelect);

// sweet alert for informations
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

// ckeditor for editorial
import CKEditor from 'ckeditor4-vue';
Vue.use(CKEditor);

// notifications
import Notifications from 'vue-notification';
Vue.use(Notifications);

/**
 * Global Components
 */
import Loader from './components/Loader';
Vue.component('loader', Loader);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
