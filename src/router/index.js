import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';
import { getToken } from '../config/api';
import routes from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  store.state.menuOpened = false;
  next();
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!getToken()) {
      window.location.href = '/giris-yap';
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

router.afterEach(to => {
  const title = document.querySelector('title');
  const titleText = to.meta && to.meta.title ? to.meta.title : 'Color Panel';
  title.innerHTML = titleText;
});

export default router;
