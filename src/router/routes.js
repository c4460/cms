// Layouts
import DashboardLayout from '../layout/DashboardLayout';

// Auth Pages
import Login from '../views/auth/Login';
import Register from '../views/auth/Register';

// Posts
import PostForm from '../views/posts/PostForm';
import PostsList from '../views/posts/PostsList';
import Categories from '../views/posts/Categories';

// Single Pages
import Dashboard from '../views/Dashboard';
import Settings from '../views/settings/index';
import CustomPage from '../views/CustomPage';
import Navigation from '../views/navigations/index';

// System Management
import SystemManagement from '../views/system/SystemManagement';

import WebsiteMenuElements from '../views/system/menu-elements/WebsiteMenuElements';
import DragDropMenuElements from '../views/system/menu-elements/DragDropMenuElements';
import MenuElementForm from '../views/system/menu-elements/MenuElementForm';
import MenuElementCustomFields from '../views/system/menu-elements/CustomFields';
import CustomFieldForm from '../views/system/menu-elements/CustomFieldForm';
import MenuElementSettingForm from '../views/system/menu-elements/SettingForm';

import WebsiteSettings from '../views/system/settings/Settings';
import WebsiteUsers from '../views/system/users/WebsiteUsers';
import Languages from '../views/system/languages/index';
import Navigations from '../views/system/navigations/index';
import API from '../views/system/api/index';

const routes = [
  {
    path: '/giris-yap',
    name: 'login',
    component: Login
  },
  {
    path: '/kayit-ol',
    name: 'register',
    component: Register
  },
  {
    path: '/uyeligi-yenileyin',
    name: 'renew-account'
  },
  {
    path: '/',
    name: 'dashboard',
    component: DashboardLayout,
    redirect: '/ayarlar',
    children: [
      {
        path: 'sistem-yonetimi',
        name: 'system-management',
        redirect: 'sistem-yonetimi/menu-elementleri',
        component: SystemManagement,
        children: [
          /**
           * Website Menu Elements
           */
          {
            path: 'eski-menu-elementleri',
            name: 'website.old-menu-elements',
            component: WebsiteMenuElements
          },
          {
            path: 'menu-elementleri',
            name: 'website.menu-elements',
            component: DragDropMenuElements
          },

          /**
           * Menu Element Custom Fields (for CRUD)
           */
          {
            path: 'ozel-alanlar/:id',
            name: 'website.menu-elements.custom-fields',
            component: MenuElementCustomFields
          },
          {
            path: 'ozel-alan-olustur/:menuElementId',
            name: 'website.menu-elements.create-custom-field',
            component: CustomFieldForm
          },
          {
            path: 'ozel-alan-duzenle/:customFieldId',
            name: 'edit-custom-field',
            component: CustomFieldForm
          },

          /**
           * Menu Element Settings (for Custom Page)
           */
          {
            path: 'ozel-sayfa-ayarlari/:id',
            name: 'website.menu-elements.settings',
            component: MenuElementCustomFields
          },
          {
            path: 'ayar-olustur/:menuElementId',
            name: 'create-setting',
            component: MenuElementSettingForm
          },
          {
            path: 'ayar-duzenle/:settingId',
            name: 'edit-setting',
            component: MenuElementSettingForm
          },

          /**
           * Menu Elements
           */
          {
            path: 'menu-elementi-olustur',
            name: 'website.create-menu-element',
            component: MenuElementForm
          },
          {
            path: 'menu-elementi-duzenle/:id',
            name: 'website.edit-menu-element',
            component: MenuElementForm
          },

          /**
           * Website Settings
           */
          {
            path: 'ayarlar',
            name: 'website.settings',
            component: WebsiteSettings
          },

          {
            path: 'kullanicilar',
            name: 'website.users',
            component: WebsiteUsers
          },

          {
            path: 'dil-yonetimi',
            name: 'website.languages',
            component: Languages
          },

          {
            path: 'navigation-yonetimi',
            name: 'website.navigations',
            component: Navigations
          },

          {
            path: 'api-ayarlari',
            name: 'website.api',
            component: API
          }
        ]
      },
      {
        path: 'anasayfa',
        name: 'home',
        component: Dashboard,
        meta: { title: 'Anasayfa' }
      },
      {
        path: ':singularSlug-olustur',
        name: 'create-post',
        component: PostForm,
        meta: { title: 'İçerik Oluştur' }
      },
      {
        path: ':singularSlug-duzenle/:id',
        name: 'edit-post',
        component: PostForm,
        meta: { title: 'İçerik Düzenle' }
      },
      {
        path: 'tum-:pluralSlug',
        name: 'posts-list',
        component: PostsList,
        meta: { title: 'Tüm İçerikler' }
      },
      {
        path: ':singularSlug-kategorileri',
        name: 'categories',
        component: Categories,
        meta: { title: 'Kategoriler' }
      },
      {
        path: 'ayarlar',
        name: 'settings',
        component: Settings,
        meta: { title: 'Ayarlar' }
      },
      {
        path: 'navigation-management',
        name: 'navigations',
        component: Navigation
      },
      {
        path: ':customSlug',
        name: 'custom-page',
        component: CustomPage
      }
    ],
    meta: { requiresAuth: true }
  }
];

export default routes;
