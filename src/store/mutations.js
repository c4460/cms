const mutations = {
  setLoading(state, payload) {
    state.loading = payload;
  },
  setAccessToken(state, token) {
    state.token = token;
  },
  setUser(state, user) {
    state.user = user;
  },
  setMenuElements(state, payload) {
    state.menuElements = payload;
  },
  setWebsite(state, website) {
    state.website = website;
  },
  setWebsites(state, payload) {
    state.websites = payload;
  },
  setMenuOpened(state, payload) {
    state.menuOpened = payload;
  },
  setLanguages(state, payload) {
    state.languages = payload;
  },
  setNavigationModalActive(state, payload) {
    state.navigationModalActive = payload;
  },
  setNavigation(state, payload) {
    state.navigation = payload;
  }
};

export default mutations;
