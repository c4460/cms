import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import getters from './getters';
import actions from './actions';
import mutations from './mutations';

export default new Vuex.Store({
  state: {
    loading: true,
    /**
     * API States
     */
    menuElements: [],

    /**
     * One Time
     */
    menuOpened: false,
    user: localStorage.getItem('user') || null,
    website: null,
    websites: [],
    languages: [],
    token: localStorage.getItem('access_token') || null,
    navigation: {},
    navigationModalActive: false
  },
  getters,
  mutations,
  actions
});
