const getters = {
  /**
   * API Getters
   */
  menuElements: state =>
    state.menuElements.length > 0 ? JSON.parse(state.menuElements) : null,

  /**
   * One Time
   */
  user: state => (state.user ? JSON.parse(state.user) : null),
  access_token: state => state.token || null,
  website: state => (state.website ? JSON.parse(state.website) : {}),
  menuOpened: state => state.menuOpened,
  websites: state => state.websites,
  languages: state => state.languages,
  loading: state => state.loading,
  navigation: state => state.navigation,
  navigationModalActive: state => state.navigationModalActive
};

export default getters;
