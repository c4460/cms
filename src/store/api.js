import axios from 'axios';
import { baseUrl } from '../config';

// Prepare axios instance
const accessToken = localStorage.getItem('access_token');
const website = localStorage.getItem('website');
const lang = localStorage.getItem('lang') || 'tr';

let request = axios.create({
  baseURL: baseUrl,
  timeout: 30000,
  headers: {
    Authorization: `Bearer ${accessToken}`,
    Website: website,
    'Access-Control-Allow-Origin': '*'
  },
  params: {
    lang
  }
});

// Do it
const api = {
  /**
   * Menu Element Custom Fields (for CRUD)
   */
  createCustomField(_, payload) {
    return request.post('custom-field/' + payload.menuElementId, payload.data);
  },
  getCustomField(_, id) {
    return request.get('custom-field/' + id);
  },
  updateCustomField(_, payload) {
    return request.put('custom-field/' + payload.id, payload.data);
  },
  deleteCustomField(_, id) {
    return request.delete('custom-field/' + id);
  },

  /**
   * Menu Element Settings (for Custom Page)
   */
  createSetting(_, payload) {
    return request.post(
      'custom-page-setting/' + payload.menuElementId,
      payload.data
    );
  },
  updateSetting(_, payload) {
    return request.put('custom-page-setting/' + payload.id, payload.data);
  },
  getSetting(_, id) {
    return request.get('custom-page-setting/' + id);
  },
  deleteCustomPageSetting(_, id) {
    return request.delete('custom-page-setting/' + id);
  },

  /**
   * Update auth user profile
   */
  updateProfile(_, payload) {
    return request.post('profile/update-user', payload);
  },
  changeMyPassword(_, payload) {
    return request.post('profile/change-password', payload);
  },

  /**
   * Current website settings
   */
  updateWebsiteSettings(_, payload) {
    return request.post('update-website-settings', payload);
  },
  getWebsiteSettings(_, params) {
    return request.get('website-settings', {
      params
    });
  },
  deleteSetting(_, id) {
    return request.post('delete-website-setting', { id: id });
  },
  changePasswordOfUser(_, payload) {
    return request.post('change-password-of-user/' + payload.id, {
      new_password: payload.password
    });
  },

  /**
   * Website user management
   */
  getWebsiteUsers() {
    return request.get('website-users');
  },
  getUser(_, id) {
    return request.get('users/' + id);
  },
  createUser(_, data) {
    return request.post('users', data);
  },
  deleteUser(_, id) {
    return request.delete('users/' + id);
  },
  updateUser(_, payload) {
    return request.put('users/' + payload.id, payload.data);
  },

  /**
   * Website Menu Element Management
   */
  websiteMenuElements() {
    return request.get('menu-elements');
  },
  getMenuElement(_, id) {
    return request.get('menu-element/' + id);
  },
  createMenuElement(_, payload) {
    return request.post('create-menu-element', payload.data);
  },
  updateMenuElement(_, data) {
    return request.put('update-menu-element/' + data.id, data.payload);
  },
  deleteMenuElement(_, id) {
    return request.delete('delete-menu-element/' + id);
  },

  // Get Menu Elements
  getWebsiteInfo({ commit }) {
    return new Promise((resolve, reject) => {
      request
        .get('website-info')
        .then(response => {
          commit('setWebsites', response.data.websites);
          commit('setLanguages', response.data.languages);
          commit('setWebsite', JSON.stringify(response.data.website));
          commit('setUser', JSON.stringify(response.data.user));
          commit(
            'setMenuElements',
            JSON.stringify(response.data.menu_elements)
          );
          commit('setLoading', false);
          localStorage.setItem('lang', response.data.languages[0].short);
          request = axios.create({
            baseURL: baseUrl,
            timeout: 30000,
            headers: {
              Authorization: `Bearer ${accessToken}`,
              Website: website,
              'Access-Control-Allow-Origin': '*'
            },
            params: {
              lang: response.data.languages[0].short
            }
          });
          resolve(true);
        })
        .catch(error => {
          reject(error.response.data);
        });
    });
  },

  // Get Settings Page Content
  settings() {
    return request.get('settings');
  },

  // PUT Update Settings
  updateSettings(_, data) {
    return request.put('settings', data);
  },

  // Get Settings Page Content
  customPage(_, slug) {
    return request.get('custom-page/' + slug);
  },

  // PUT Update Settings
  updateCustomPage(_, data) {
    return request.put('custom-page/' + data.slug, data.request);
  },

  // Image Upload
  imageUpload(_, formData) {
    return request.post('image-upload-s3', formData);
  },

  // File Upload
  fileUpload(_, formData) {
    return request.post('file-upload-s3', formData);
  },

  // Get Posts List
  postsList(_, data) {
    return request.get('posts', {
      params: {
        page: data.page || 1,
        plural_slug: data.plural_slug
      }
    });
  },

  // Get Single Menu Element
  menuElement(_, slug) {
    return request.get('menu-element', {
      params: { singular_slug: slug }
    });
  },

  // GET Create Post Page Dependencies
  createPost(_, slug) {
    return request.get('posts/create', {
      params: { singular_slug: slug }
    });
  },

  // POST Set Post Slug
  postSlugFromTitle(_, title) {
    return request.post('posts/slug-from-title', {
      title
    });
  },

  // POST Set Post Slug
  postSlugFromSlug(_, slug) {
    return request.post('posts/slug-from-slug', {
      slug
    });
  },

  // GET Edit Post Page Dependencies
  editPost(_, id) {
    return request.get('posts/' + id);
  },

  // POST Store New Post
  storePost(_, data) {
    return request.post('posts', data);
  },

  // PUT Update The Post
  updatePost(_, data) {
    return request.put('posts/' + data.id, {
      ...data.post,
      custom_fields: data.custom_fields
    });
  },

  // DELETE Post
  deletePost(_, id) {
    return request.delete('posts/' + id);
  },

  // POST Delete Post Image
  deletePostImage(_, id) {
    return request.post('delete-post-image', {
      id
    });
  },

  // GET Categories Page Dependencies
  categories(_, singularSlug) {
    return request.get('categories-from/' + singularSlug);
  },

  // POST Store New Category
  storeCategory(_, data) {
    return request.post('categories', {
      name: data.category.name,
      parent_id: data.category.parent_id,
      element_id: data.element_id
    });
  },

  // PUT Update The Category
  updateCategory(_, data) {
    return request.put('categories/' + data.id, data);
  },

  // DELETE Category
  deleteCategory(_, id) {
    return request.delete('categories/' + id);
  },

  // Super User Actions
  updateSortsOfSettings(_, payload) {
    return request.put('update-sorts-of-settings', payload);
  },
  updateSortsOfCustomFields(_, payload) {
    return request.put('update-sorts-of-custom-fields', payload);
  },
  sortMenuElements(_, payload) {
    return request.post('sort-menu-elements', payload);
  },
  createWebsite(_, payload) {
    return request.post('create-website', payload);
  },
  getThemes() {
    return request.get('themes');
  },
  copyWebsite(_, payload) {
    return request.post('copy-website', payload);
  },
  deleteWebsite(_, payload) {
    return request.post('delete-website', payload);
  },
  changeUserRole(_, payload) {
    return request.put('update-user-role', payload);
  },
  addUser(_, payload) {
    return request.post('add-user', payload);
  },
  removeUser(_, id) {
    return request.delete('website-user/' + id);
  },
  getLanguages() {
    return request.get('languages');
  },
  createLanguage(_, payload) {
    return request.post('languages', payload);
  },
  updateLanguage(_, payload) {
    return request.put('languages/' + payload.id, payload.data);
  },
  deleteLanguage(_, id) {
    return request.delete('languages/' + id);
  },
  listNavigations() {
    return request.get('navigations');
  },
  createNavigation(_, payload) {
    return request.post('navigation', payload);
  },
  updateNavigation(_, payload) {
    return request.put('navigation/' + payload.id, payload.data);
  },
  deleteNavigation(_, id) {
    return request.delete('navigation/' + id);
  },
  updateNavigationData(_, payload) {
    return request.put('update-navigation-value/' + payload.id, payload.data);
  },

  getWebsiteApiKeys() {
    return request.get('website-api-keys');
  },
  refreshApiKeys() {
    return request.post('refresh-api-keys');
  }
};

export default api;
