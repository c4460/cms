import api from './api';
import axios from 'axios';
import { baseUrl } from '../config';

const actions = {
  /**
   * Requests
   */
  ...api,

  /**
   *
   */
  // Mobile sidebar menu
  setMenuOpened({ commit }, payload) {
    commit('setMenuOpened', payload);
  },

  // Login action
  loginAction({ commit }, request) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          baseUrl + 'login',
          {
            email: request.email,
            password: request.password
          },
          {
            headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*'
            }
          }
        )
        .then(res => res.data)
        .then(response => {
          commit('setAccessToken', response.access_token);
          localStorage.setItem('access_token', response.access_token);
          commit('setWebsite', response.website);
          localStorage.setItem('website', response.website);
          resolve(true);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // Login action
  registerAction({ commit }, request) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          baseUrl + 'register',
          {
            title: request.title,
            name: request.name,
            email: request.email,
            password: request.password
          },
          {
            headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*'
            }
          }
        )
        .then(res => res.data)
        .then(response => {
          commit('setAccessToken', response.access_token);
          localStorage.setItem('access_token', response.access_token);
          commit('setWebsite', response.website);
          localStorage.setItem('website', response.website);
          resolve(true);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  // Set User
  setUser({ commit }, payload) {
    commit('setUser', payload);
    localStorage.setItem('user', JSON.stringify(payload));
  },

  // Logout Action
  logout() {
    return new Promise(resolve => {
      localStorage.removeItem('access_token');
      localStorage.removeItem('user');
      localStorage.removeItem('website');
      resolve(true);
    });
  }
};

export default actions;
