export const baseUrl =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost/api/'
    : 'https://colortasarim.com/api/';
