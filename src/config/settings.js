/**
 * Social Media Settings
 */
export const socialMediaSettings = {
  facebook: {
    type: 'text',
    label: 'Facebook',
    value: '',
    placeholder: 'https://facebook.com/username'
  },
  twitter: {
    type: 'text',
    label: 'Twitter',
    value: '',
    placeholder: 'https://twitter.com/username'
  },
  instagram: {
    type: 'text',
    label: 'Instagram',
    value: '',
    placeholder: 'https://instagram.com/username'
  },
  youtube: {
    type: 'text',
    label: 'Youtube',
    value: '',
    placeholder: 'https://youtube.com/username'
  },
  linkedin: {
    type: 'text',
    label: 'Linkedin',
    value: '',
    placeholder: 'https://linkedin.com/in/username'
  },
  dribble: {
    type: 'text',
    label: 'Dribble',
    value: '',
    placeholder: 'https://dribble.com/username'
  },
  behance: {
    type: 'text',
    label: 'Behance',
    value: '',
    placeholder: 'https://behance.com/username'
  },
  github: {
    type: 'text',
    label: 'Github',
    value: '',
    placeholder: 'https://github.com/username'
  },
  skype: {
    type: 'text',
    label: 'Skype',
    value: '',
    placeholder: 'username'
  }
};

/**
 * General Settings
 */
export const generalSettings = {
  maps_iframe_url: {
    type: 'text',
    label: 'Maps Iframe URL',
    value: ''
  },
  phone: {
    type: 'text',
    label: 'Telefon',
    value: ''
  },
  phone2: {
    type: 'text',
    label: 'Telefon 2',
    value: ''
  },
  email: {
    type: 'text',
    label: 'E-mail',
    value: ''
  },
  address: {
    type: 'text',
    label: 'Adres',
    value: ''
  },
  headTag: {
    type: 'textarea',
    label: 'Head',
    value: ''
  },
  bodyTag: {
    type: 'textarea',
    label: 'Body',
    value: ''
  },
  externalCSS: {
    type: 'textarea',
    label: 'Özel CSS',
    value: ''
  },
  googleRecaptchKey: {
    type: 'text',
    label: 'Google Recaptcha Site Key',
    value: ''
  },
  googleRecaptchSecret: {
    type: 'text',
    label: 'Google Recaptcha Secret Key',
    value: ''
  }
};
