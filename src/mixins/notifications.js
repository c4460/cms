const notifications = {
  methods: {
    success(message, title = 'Başarılı!') {
      this.$notify({
        type: 'success',
        title: title,
        text: message
      });
    },
    error(message, title = 'Hata!') {
      this.$notify({
        type: 'error',
        title: title,
        text: message
      });
    }
  }
};

export default notifications;
