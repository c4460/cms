import axios from 'axios';
import { baseUrl } from './config';

// Prepare axios instance
const accessToken = localStorage.getItem('access_token');
const website = localStorage.getItem('website');
const lang = localStorage.getItem('lang') || 'tr';

const request = axios.create({
  baseURL: baseUrl,
  timeout: 30000,
  headers: {
    Authorization: `Bearer ${accessToken}`,
    Website: website,
    'Access-Control-Allow-Origin': '*'
  },
  params: {
    lang
  }
});

export default request;
